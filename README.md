## Requirements

This version of application requires the following dependencies

| Name          | Details                  |
|---------------|--------------------------|
| IDE           | Intellij IDE             |
| Java Version  | Java 8                   |
| Framework     | Spring Boot              |
| Database      | MySQL                    |

## Import Data

Look at package resources -> db -> migration on the project. We are using flyway to migrate the database

## Postman Documentation

https://documenter.getpostman.com/view/1388924/2sA3XWeKBF#intro

## How to Run

Run registry and library app first. Then While using Intellij IDE, simply just click Run Button.