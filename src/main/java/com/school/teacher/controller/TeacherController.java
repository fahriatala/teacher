package com.school.teacher.controller;

import com.school.teacher.model.request.TeacherRequest;
import com.school.teacher.model.response.TeacherResponse;
import com.school.teacher.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping
    public ResponseEntity<List<TeacherResponse>> getAllTeachers() {
        List<TeacherResponse> teachers = teacherService.getAllTeachers();
        return ResponseEntity.ok(teachers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TeacherResponse> getTeacherById(@PathVariable Long id) {
        TeacherResponse teacher = teacherService.getTeacherById(id);
        return ResponseEntity.ok(teacher);
    }

    @PostMapping
    public ResponseEntity<Void> insertTeacher(@RequestBody TeacherRequest request) throws Exception {
        teacherService.insertTeacher(request);
        return ResponseEntity.status(201).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateTeacher(@PathVariable Long id, @RequestBody TeacherRequest request) {
        teacherService.updateTeacher(id, request);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/soft-delete/{id}")
    public ResponseEntity<Void> deleteTeacher(@PathVariable Long id) {
        teacherService.deleteTeacher(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/exists/{teacherId}")
    public boolean checkTeacherExists(@PathVariable long teacherId) {
        return teacherService.checkTeacherExistsById(teacherId);
    }
}
