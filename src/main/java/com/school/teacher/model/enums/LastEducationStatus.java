package com.school.teacher.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public enum LastEducationStatus {
    DIPLOMA("1"),
    BACHELOR("2"),
    MASTER("3"),
    DOCTORAL("4");

    private final String value;
    private static final Map<String, LastEducationStatus> VALUE_MAP = new HashMap<>();

    static {
        for (LastEducationStatus status : LastEducationStatus.values()) {
            VALUE_MAP.put(status.getValue(), status);
        }
    }

    public static LastEducationStatus fromValue(String value) {
        return VALUE_MAP.get(value);
    }

    public static boolean isValid(String value) {
        return VALUE_MAP.containsKey(value);
    }
}
