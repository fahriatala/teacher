package com.school.teacher.service;

import com.school.teacher.model.request.TeacherRequest;
import com.school.teacher.model.response.TeacherResponse;

import java.util.List;

public interface TeacherService {
    List<TeacherResponse> getAllTeachers();
    TeacherResponse getTeacherById(Long id);
    void insertTeacher(TeacherRequest request) throws Exception;
    void updateTeacher(Long id, TeacherRequest request);
    void deleteTeacher(Long id);
    boolean checkTeacherExistsById(long teacherId);
}
