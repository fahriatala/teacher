package com.school.teacher.service;

import com.school.teacher.model.entity.Teacher;
import com.school.teacher.model.enums.LastEducationStatus;
import com.school.teacher.model.request.TeacherRequest;
import com.school.teacher.model.response.TeacherResponse;
import com.school.teacher.repository.TeacherRepository;
import com.shared.library.AppException;
import com.shared.library.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class TeacherServiceImpl implements TeacherService  {
    @Autowired
    private TeacherRepository teacherRepository;

    private static final String TeacherNotFoundText = "Teacher not found for id ";

    @Override
    public List<TeacherResponse> getAllTeachers() {
        return teacherRepository.findAllByDeletedAtIsNull().stream()
                .map(this::convertToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public TeacherResponse getTeacherById(Long id) {
        Teacher teacher = teacherRepository.findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new DataNotFoundException(TeacherNotFoundText + id));

        return convertToResponse(teacher);
    }

    @Override
    public void insertTeacher(TeacherRequest request) throws Exception {
        if (!LastEducationStatus.isValid(request.getLastEducation())) {
            throw new AppException("Invalid Last Education");
        }

        Teacher teacher = new Teacher();
        teacher.setFullName(request.getFullName());
        teacher.setEmail(request.getEmail());
        teacher.setPhoneNumber(request.getPhoneNumber());
        teacher.setAddress(request.getAddress());
        teacher.setDateOfBirth(request.getDateOfBirth());
        teacher.setPlaceOfBirth(request.getPlaceOfBirth());
        teacher.setUniversity(request.getUniversity());
        teacher.setLastEducation(request.getLastEducation());
        teacher.setSubject(request.getSubject());
        teacher.setJoinDate(request.getJoinDate());

        teacherRepository.save(teacher);
    }

    @Override
    public void updateTeacher(Long id, TeacherRequest request) {
        Teacher teacher = teacherRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(TeacherNotFoundText + id));

        if (!LastEducationStatus.isValid(request.getLastEducation())) {
            throw new AppException("Invalid Last Education");
        }

        teacher.setFullName(request.getFullName() != null ? request.getFullName() : teacher.getFullName());
        teacher.setEmail(request.getEmail() != null ? request.getEmail() : teacher.getEmail());
        teacher.setPhoneNumber(request.getPhoneNumber() != null ? request.getPhoneNumber() : teacher.getPhoneNumber());
        teacher.setAddress(request.getAddress() != null ? request.getAddress() : teacher.getAddress());
        teacher.setDateOfBirth(request.getDateOfBirth() != null ? request.getDateOfBirth() : teacher.getDateOfBirth());
        teacher.setPlaceOfBirth(request.getPlaceOfBirth() != null ? request.getPlaceOfBirth() : teacher.getPlaceOfBirth());
        teacher.setUniversity(request.getUniversity() != null ? request.getUniversity() : teacher.getUniversity());
        teacher.setLastEducation(request.getLastEducation() != null ? request.getLastEducation() : teacher.getLastEducation());
        teacher.setSubject(request.getSubject() != null ? request.getSubject() : teacher.getSubject());
        teacher.setJoinDate(request.getJoinDate() != null ? request.getJoinDate() : teacher.getJoinDate());
        teacher.setId(id);

        teacherRepository.save(teacher);
    }

    @Override
    public void deleteTeacher(Long id) {
        Teacher teacher = teacherRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(TeacherNotFoundText + id));

        teacher.setDeletedAt(LocalDateTime.now());
        teacherRepository.save(teacher);
    }

    @Override
    public boolean checkTeacherExistsById(long teacherId) {
        return teacherRepository.existsById(teacherId);
    }

    private TeacherResponse convertToResponse(Teacher teacher) {
        TeacherResponse response = new TeacherResponse();
        response.setId(teacher.getId());
        response.setFullName(teacher.getFullName());
        response.setEmail(teacher.getEmail());
        response.setPhoneNumber(teacher.getPhoneNumber());
        response.setAddress(teacher.getAddress());
        response.setDateOfBirth(teacher.getDateOfBirth());
        response.setPlaceOfBirth(teacher.getPlaceOfBirth());
        response.setUniversity(teacher.getUniversity());
        response.setLastEducation(teacher.getLastEducation());
        response.setSubject(teacher.getSubject());
        response.setJoinDate(teacher.getJoinDate());

        String lastEducation = teacher.getLastEducation();
        String educationStatus = (lastEducation != null ? LastEducationStatus.fromValue(lastEducation).name() : null);
        response.setLastEducation(educationStatus);
        return response;
    }
}
