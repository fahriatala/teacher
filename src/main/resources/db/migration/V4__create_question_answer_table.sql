CREATE TABLE question_answer (
    id INT AUTO_INCREMENT PRIMARY KEY,
    question_id INT,
    student_id INT,
    question_answer TEXT NOT NULL,
    score DECIMAL(5, 2) null,  -- Allows up to 3 digits in total, with 2 digits after the decimal point
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL,
    FOREIGN KEY (question_id) REFERENCES question(id),
    FOREIGN KEY (student_id) REFERENCES students(id)
);