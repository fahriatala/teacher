package com.school.teacher;

import com.school.teacher.model.entity.Teacher;
import com.school.teacher.model.response.TeacherResponse;
import com.school.teacher.repository.TeacherRepository;
import com.school.teacher.service.TeacherServiceImpl;
import com.shared.library.DataNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class GetTeacherByIdTest {
    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    public GetTeacherByIdTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetTeacherById_WhenTeacherFound() {
        Long id = 1L;
        Teacher teacher = new Teacher();
        teacher.setId(id);
        teacher.setFullName("John Doe");

        when(teacherRepository.findByIdAndDeletedAtIsNull(id)).thenReturn(Optional.of(teacher));
        TeacherResponse response = teacherService.getTeacherById(id);

        Assertions.assertNotNull(response);
        Assertions.assertEquals("John Doe", response.getFullName());
    }

    @Test
    void testGetTeacherById_WhenTeacherNotFound() {
        when(teacherRepository.findByIdAndDeletedAtIsNull(any())).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> {
            teacherService.getTeacherById(1L);
        });
    }
}
