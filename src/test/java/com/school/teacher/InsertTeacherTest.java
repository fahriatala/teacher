package com.school.teacher;

import com.school.teacher.model.entity.Teacher;
import com.school.teacher.model.request.TeacherRequest;
import com.school.teacher.repository.TeacherRepository;
import com.school.teacher.service.TeacherServiceImpl;
import com.shared.library.AppException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;

class InsertTeacherTest {
    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;

    @Captor
    private ArgumentCaptor<Teacher> teacherArgumentCaptor;

    public InsertTeacherTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testInsertTeacher_ValidRequest() throws Exception {
        TeacherRequest request = new TeacherRequest();
        request.setFullName("John Doe");
        request.setEmail("john@example.com");
        request.setPhoneNumber("123456789");
        request.setAddress("123 Street, City");
        request.setDateOfBirth(LocalDate.of(1981, 3, 4));
        request.setPlaceOfBirth("City");
        request.setUniversity("University");
        request.setLastEducation("diploma"); // Valid last education
        request.setSubject("Math");
        request.setLastEducation("1");
        request.setJoinDate(LocalDate.of(2008, 4, 5));

        teacherService.insertTeacher(request);

        verify(teacherRepository).save(teacherArgumentCaptor.capture());
        Teacher teacher = teacherArgumentCaptor.getValue();
        Assertions.assertEquals("John Doe", teacher.getFullName());
    }

    @Test
    void testInsertTeacher_InvalidLastEducation() throws Exception {
        TeacherRequest request = new TeacherRequest();
        request.setLastEducation("invalid");

        assertThrows(AppException.class, () -> {
            teacherService.insertTeacher(request);
        });
    }
}
